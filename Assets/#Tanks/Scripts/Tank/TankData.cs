﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {

    public float fuel;
    public float healthy;
    public int bulletsCapacity;
    public AudioSource m_ShootingAudio;         // Reference to the audio source used to play the shooting audio. NB: different to the movement audio source.
    public AudioClip m_ChargingClip;            // Audio that plays when each shot is charging up.
    public AudioClip m_FireClip;                // Audio that plays when each shot is fired.


    float fuelValue;
    float healthyValue;
    int bulletValue;

    // Use this for initialization
    void Start () {

        fuelValue = fuel;
        healthyValue = healthy;
        bulletValue = bulletsCapacity;
    }
	
	// Update is called once per frame
	void Update () {

        fuel -= Time.deltaTime;

       // Debug.Log("Tanks: " + fuel + "healthy: " + healthy);

        if (fuel <= 0) fuel = 0;
        if (healthy <= 0) healthy = 0;
	}

    public void FillTheTankFuel()
    {
        fuel = fuelValue;
    }

    public void RestoreHealthy()
    {
        healthy = healthyValue;
    }

    public void RechargeWeapon()
    {
        bulletsCapacity = bulletValue;
        // Change the clip to the charging clip and start it playing.
        m_ShootingAudio.clip = m_ChargingClip;
        m_ShootingAudio.Play();
    }

    public void BulletFired()
    {
        if (bulletsCapacity > 0) bulletsCapacity--;
        
        // Change the clip to the firing clip and play it.
        m_ShootingAudio.clip = m_FireClip;
        m_ShootingAudio.Play();
       
    }

    public bool IsRunOutOfbullets()
    {
        if (bulletsCapacity > 0) return false;
        else return true;
    }

    public void TakeDamage(float damage)
    {
        healthy -= damage;
        if (healthy < 0) healthy = 0;
    }

    public bool IsDead()
    {
        if (healthy <= 0) return true;
        else return false;
    }

}
