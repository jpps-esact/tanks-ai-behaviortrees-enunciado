﻿using Pada1.BBCore.Framework;
using Pada1.BBCore;

namespace BBUnity.Conditions
{
    /// <summary>
    /// It is a basic action to associate a Boolean to a variable.
    /// </summary>
    [Condition("Basic/IsDay")]
    [Help("Sets a value to a boolean variable")]
    public class IsDay : GOCondition
    {
    
        /// <summary>
        /// Checks if the target healthy depending on a given healthy,
        /// </summary>
        /// <returns>True if the healthy is higher  thatn the given healthy.</returns>
        public override bool Check()
        {
            return ((UnityEngine.Random.Range(0, 1) > 0) ? true : false);
        }

    }
}
